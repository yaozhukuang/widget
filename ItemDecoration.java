package com.buy8.online.shopping.order.adapter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author zhanwei
 * @date 2020/2/20
 * Email zhanwei@xiaomi.com
 */
public class ItemDecoration extends RecyclerView.ItemDecoration {

    private int divide;
    private final Rect mBounds = new Rect();
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int offset;

    public ItemDecoration(int divide) {
        this(divide, Color.TRANSPARENT);
    }

    public ItemDecoration(int divide, @ColorInt int color) {
        this(divide, Color.TRANSPARENT, 0);
    }

    public ItemDecoration(int divide, @ColorInt int color, int offset) {
        this.divide = divide;
        this.offset = offset;
        mPaint.setColor(color);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager manager = parent.getLayoutManager();
        if (manager == null) {
            return;
        }
        int position = manager.getPosition(view);
        if (position < offset) {
            return;
        }
        if (manager instanceof GridLayoutManager) {
            int spanCount = ((GridLayoutManager) manager).getSpanCount();
            if (spanCount < 2) {
                return;
            }
            int pos = parent.getChildAdapterPosition(view);
            int column = (pos - offset) % spanCount;// 计算这个child 处于第几列

            outRect.bottom = divide;
            //注意这里一定要先乘 后除  先除数因为小于1然后强转int后会为0
            outRect.left = column * divide / spanCount; //左侧为(当前条目数-1)/总条目数*divider宽度
            outRect.right = (spanCount - 1 - column) * divide / spanCount;//右侧为(总条目数-当前条目数)/总条目数*divider宽度

            return;
        }
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) manager;
        if (linearLayoutManager.canScrollHorizontally()) {
            outRect.right = divide;
        } else {
            outRect.bottom = divide;
        }
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDraw(c, parent, state);
        RecyclerView.LayoutManager manager = parent.getLayoutManager();
        if (manager == null) {
            return;
        }
        if (manager instanceof GridLayoutManager) {
            drawGrid(c, parent);
            return;
        }
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) manager;
        if (linearLayoutManager.canScrollHorizontally()) {
            drawHorizontal(c, parent);
        } else {
            drawVertical(c, parent);
        }
    }

    private void drawGrid(Canvas canvas, RecyclerView parent) {
        RecyclerView.LayoutManager manager = parent.getLayoutManager();
        if (manager == null) {
            return;
        }

        canvas.save();
        final int childCount = parent.getChildCount();
        int spanCount = ((GridLayoutManager) manager).getSpanCount();
        for (int i = offset; i < childCount; i++) {
            int index = (i - offset) % spanCount;
            if (index < spanCount) {
                final View child = parent.getChildAt(i);
                canvas.drawRect(child.getRight(), child.getTop(), child.getRight() + divide, child.getBottom() + divide, mPaint);
                canvas.drawRect(child.getLeft(), child.getBottom(), child.getRight(), child.getBottom() + divide, mPaint);
            }
        }
    }

    private void drawVertical(Canvas canvas, RecyclerView parent) {
        canvas.save();
        final int left;
        final int right;
        if (parent.getClipToPadding()) {
            left = parent.getPaddingLeft();
            right = parent.getWidth() - parent.getPaddingRight();
            canvas.clipRect(left, parent.getPaddingTop(), right,
                    parent.getHeight() - parent.getPaddingBottom());
        } else {
            left = 0;
            right = parent.getWidth();
        }

        final int childCount = parent.getChildCount();
        for (int i = offset; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, mBounds);
            canvas.drawRect(left, mBounds.bottom, right, mBounds.bottom + divide, mPaint);
        }
        canvas.restore();
    }

    private void drawHorizontal(Canvas canvas, RecyclerView parent) {
        canvas.save();
        final int top;
        final int bottom;
        if (parent.getClipToPadding()) {
            top = parent.getPaddingTop();
            bottom = parent.getHeight() - parent.getPaddingBottom();
            canvas.clipRect(parent.getPaddingLeft(), top,
                    parent.getWidth() - parent.getPaddingRight(), bottom);
        } else {
            top = 0;
            bottom = parent.getHeight();
        }

        final int childCount = parent.getChildCount();
        for (int i = offset; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, mBounds);
            canvas.drawRect(mBounds.right, top, mBounds.right + divide, bottom, mPaint);
        }
        canvas.restore();
    }
}
